const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

module.exports.addProduct = (prod) => {

	let newProduct = new Product({
		name: prod.name,
		description: prod.description,
		price: prod.price
	});

	return newProduct.save().then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return true;
		}
	})
};

module.exports.availableProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}
