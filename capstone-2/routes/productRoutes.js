const express = require("express");
const router = express.Router();
const prodController = require("../controller/prodController.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		prodController.addProduct(data.product).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

router.get("/active", (req, res) => {
	prodController.availableProducts().then(resultFromController => res.send(resultFromController));
})

module.exports = router;