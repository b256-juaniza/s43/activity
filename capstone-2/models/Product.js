const mongoose = require("mongoose");

const prodSchema = new mongoose.Schema({
	name: {
		type: String,
	},
	description: {
		type: String
	},
	price: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
	{
		userId: {
			type: String
		},
		orderId: {
			type: String
		}
	}]
})

module.exports = mongoose.model("Product", prodSchema);